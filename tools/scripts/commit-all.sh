#!/bin/sh

cd core
echo " " && echo ">> PWD =" $PWD
git add .
git commit -am "Updated"
git push
cd ..

cd lambda
echo " " && echo ">> PWD =" $PWD
git add .
git commit -am "Updated"
git push
cd ..

cd client
echo " " && echo ">> PWD =" $PWD
git add .
git commit -am "Updated"
git push
cd ..

